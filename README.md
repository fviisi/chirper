# Chirper #

A REST API for saving and retrieving chirps. Used a simple Derby database for persisting the chirps. Also used Maven and Spring boot.

Struggled to create a complete solution in the time given, therefore it is quite hard coded and without proper test coverage.