package chirper;

import java.sql.SQLException;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChirpController {

	private String dbUrl = "jdbc:derby:chirper;create=true";

	private ChirpDAO chirpDAO = new ChirpDAO(dbUrl);

	@RequestMapping("/retrieve/{userName}")
	public List<Chirp> retrieve(@PathVariable String userName) throws SQLException {
		return chirpDAO.retrieveChirpByUser(userName);
	}

	@RequestMapping("/save/{user}/{text}")
	public String save(@PathVariable String user, @PathVariable String text) {
		Chirp chirp = new Chirp();
		chirp.setUserName(user);
		chirp.setChirpText(text);
		try {
			chirpDAO.saveChirp(chirp);
		} catch (SQLException e) {
			e.printStackTrace();
			return "Failed!";
		}
		return "OK!";
	}
}
