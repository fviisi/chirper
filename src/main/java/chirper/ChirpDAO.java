package chirper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ChirpDAO {

	private static final String STATEMENT_NEXT_SEQ_VALUE = "next value FOR chirp_seq";
	private static final String STATEMENT_INSERT_INTO_CHIRP = "insert into chirp(id, chirpText, userName) values (";
	private static final String COLUMN_NAME_USER_NAME = "userName";
	private static final String COLUMN_NAME_CHIRP_TEXT = "chirpText";
	private static final String COLUMN_NAME_ID = "id";
	private static final String STATEMENT_SELECT_BY_USERNAME = "select * from chirp where userName like ";
	private static final String CHIRP_TABLE_NAME_CAPS = "CHIRP";
	private static final String STATEMENT_CREATE_SEQUENCE = "Create sequence chirp_seq as int maxvalue 999999 start with 1";
	private static final String STATEMENT_CREATE_CHIRP_TABLE = "Create table chirp (id int primary key, userName varchar(30), chirpText varchar(280))";

	private String dbUrl;

	public ChirpDAO(String dbUrl) {
		super();
		this.dbUrl = dbUrl;
		try {
			createDatabase();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void saveChirp(Chirp chirp) throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append(STATEMENT_INSERT_INTO_CHIRP);
		builder.append(STATEMENT_NEXT_SEQ_VALUE);
		builder.append(",");
		builder.append("'" + chirp.getChirpText() + "'");
		builder.append(",");
		builder.append("'" + chirp.getUserName() + "')");
		executeStatement(builder.toString());
	}

	public void retrieveChirp(Long id) {

	}

	public List<Chirp> retrieveChirpByUser(String userName) throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append(STATEMENT_SELECT_BY_USERNAME);
		builder.append("'%" + userName + "%'");
		ResultSet rs = executeQuery(builder.toString());
		if (!rs.next()) {
			return new ArrayList<>();
		}
		return resultSetToList(rs);
	}

	private void createDatabase() throws SQLException {
		Connection connection = DriverManager.getConnection(dbUrl);
		if (!connection.getMetaData().getTables(null, null, CHIRP_TABLE_NAME_CAPS, new String[] { "TABLE" }).next()) {
			executeStatement(STATEMENT_CREATE_CHIRP_TABLE);
			executeStatement(STATEMENT_CREATE_SEQUENCE);
		}
	}

	private void executeStatement(String statement) throws SQLException {
		Connection connection = DriverManager.getConnection(dbUrl);
		Statement stmt = connection.createStatement();
		try {
			stmt.executeUpdate(statement);
		} finally {
			stmt.close();
		}
	}

	private ResultSet executeQuery(String statement) throws SQLException {
		Connection connection = DriverManager.getConnection(dbUrl);
		connection.setAutoCommit(false);
		Statement stmt = connection.createStatement();
		try {
			return stmt.executeQuery(statement);
		} finally {
			connection.setAutoCommit(true);
		}
	}

	private List<Chirp> resultSetToList(ResultSet rs) throws SQLException {
		List<Chirp> chirps = new ArrayList<>();
		while (rs.next()) {
			Chirp chirp = new Chirp();
			chirp.setId(rs.getLong(COLUMN_NAME_ID));
			chirp.setChirpText(rs.getString(COLUMN_NAME_CHIRP_TEXT));
			chirp.setUserName(rs.getString(COLUMN_NAME_USER_NAME));
			chirps.add(chirp);
		}
		return chirps;
	}

}
