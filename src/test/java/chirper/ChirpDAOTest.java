package chirper;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Before;
import org.junit.Test;

public class ChirpDAOTest {

	private Connection connection;

	private ChirpDAO chirpDAO;

	@Before
	public void setup() throws SQLException {
		String dbUrl = "jdbc:derby:memory:demo;create=true";
		connection = DriverManager.getConnection(dbUrl);
		chirpDAO = new ChirpDAO(dbUrl);
	}

	@Test
	public void testSaveChirp() throws SQLException {
		Chirp chirp = new Chirp();
		chirp.setChirpText("text");
		chirp.setUserName("name");
		chirpDAO.saveChirp(chirp);
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("select * from chirp");
		while (rs.next()) {
			assertEquals(1L, rs.getLong("id"));
			assertEquals("text", rs.getString("chirpText"));
			assertEquals("name", rs.getString("userName"));
		}
	}

	// Add more tests!!

}
